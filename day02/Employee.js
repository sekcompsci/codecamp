class Employee {
    constructor(firstname, lastname, salary) {
        this._firstname = firstname;
        this._lastname = lastname;
        this._salary = salary; // simulate private variable
    }
    setSalary(newSalary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        if (newSalary > this._salary)
            return newSalary
        else return false;
    }
    getSalary () {  // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
        console.log(`${employee._firstname}  has been fired! Dress with :tshirt`);
    }
    leaveForVacation(year, month, day) {

    }
}

module.exports = Employee;