import React, { Component } from 'react';
import { Card, Select, Layout, Row, Col, Icon } from 'antd';
import axios from 'axios';
import moment from 'moment';

const Option = Select.Option;
const { Header, Content } = Layout;

class Homework1 extends Component {
    state = {
        val: null,
        type: 'USD',
        interval: null,
        lastUpdate: null
    }

    async getData(type) {
        const val = await axios.get('https://api.coindesk.com/v1/bpi/currentprice.json')

        if (val) {
            this.setState({ val: val.data.bpi[type].rate, lastUpdate: moment().format('MMMM Do YYYY, h:mm:ss a') })
        }
    }

    typeChange = (type) => {
        this.setState({ val: null, lastUpdate: null, type: type })
        this.getData(type)
    }

    update = () => {
        this.getData(this.state.type)
        console.log('update')
    }

    loader = () => {
        return ( <Icon type="loading" />)
    }

    componentDidMount() {
        this.getData(this.state.type)
        
        const interval = setInterval(this.update, 5000)
        this.setState({ interval: interval })
    }

    componentWillUnmount() {
        clearInterval(this.state.interval)
    }

    render() {
        return (
            <Layout className="layout">
                <Header style={{ color: '#FFF' }}>Code Camp</Header>
                <Content>
                    <Row>
                        <Col span={8} offset={8}>
                            <Card className="bitcoin" title="Bitcoin price checker" extra={
                                <Select value={this.state.type} onChange={this.typeChange}>
                                    <Option value="USD">USD</Option>
                                    <Option value="GBP">GBP</Option>
                                    <Option value="EUR">EUR</Option>
                                </Select>
                            }>
                                <p>current price: <b>{this.state.val?(this.state.val + ' ' + this.state.type):this.loader()}</b></p>
                                <p>last update: {this.state.lastUpdate?this.state.lastUpdate:this.loader()}</p>
                            </Card>
                        </Col>
                    </Row>
                </Content>
            </Layout>
        )
    }
}

export default Homework1;