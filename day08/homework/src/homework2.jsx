import React, { Component } from 'react';
import { Card, Select, Layout, Row, Col, Icon } from 'antd';
import { LineChart, Line, Legend, Tooltip, CartesianGrid, XAxis, YAxis } from 'recharts';
import axios from 'axios';

const Option = Select.Option;
const { Header, Content } = Layout;

class Homework2 extends Component {
    state = {
        val: null,
        data: null,
        type: 'USD',
        interval: null
    }

    async getData(type) {
        const hw1 = await axios.get('https://api.coindesk.com/v1/bpi/currentprice.json')
        const hw2 = await axios.get('https://api.coindesk.com/v1/bpi/historical/close.json?currency=' + this.state.type)

        if (hw1) {
            this.setState({ val: hw1.data.bpi[type].rate })
        }

        if (hw2) {
            const arr = [];
            for (let key in hw2.data.bpi) {
                arr.push({ date: key, price: hw2.data.bpi[key] })
            }

            this.setState({ data: arr })
        }
    }

    typeChange = (type) => {
        this.setState({ val: null, type: type })
        this.getData(type)
    }

    update = () => {
        this.getData(this.state.type)
        console.log('update')
    }

    loader = () => {
        return (<Icon type="loading" />)
    }

    componentDidMount() {
        this.getData(this.state.type)

        const interval = setInterval(this.update, 5000)
        this.setState({ interval: interval })
    }

    componentWillUnmount() {
        clearInterval(this.state.interval)
    }

    render() {
        return (
            <Layout className="layout">
                <Header style={{ color: '#FFF' }}>Code Camp</Header>
                <Content>
                    <Row>
                        <Col span={12} offset={6}>
                            <Card className="bitcoin" title="Bitcoin price checker" extra={
                                <Select value={this.state.type} onChange={this.typeChange}>
                                    <Option value="USD">USD</Option>
                                    <Option value="GBP">GBP</Option>
                                    <Option value="EUR">EUR</Option>
                                </Select>
                            }>
                                <p style={{ textAlign:"right" }}>current price: <b>{this.state.val ? (this.state.val + ' ' + this.state.type) : this.loader()}</b></p>
                                <LineChart width={550} height={250} data={this.state.data}
                                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                                    <XAxis dataKey="date" />
                                    <YAxis dataKey="price" />
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <Tooltip />
                                    <Legend verticalAlign="bottom" height={36} />
                                    <Line name={this.state.type} type="monotone" dataKey="price" stroke="green" />
                                </LineChart>
                            </Card>
                        </Col>
                    </Row>
                </Content>
            </Layout>
        )
    }
}

export default Homework2;