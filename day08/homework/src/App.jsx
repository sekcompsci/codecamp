import React, { Component } from 'react';
// import Homework1 from './homework1';
// import Homework2 from './homework2';
import Homework3 from './homework3';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        {/* <Homework1 /> */}
        {/* <Homework2 /> */}
        <Homework3 />
      </div>
    );
  }
}

export default App;
