let peopleSalary = [
  {"id":"1001","firstname":"Luke","lastname":"Skywalker","company":"Walt Disney","salary":40000},
  {"id":"1002","firstname":"Tony","lastname":"Stark","company":"Marval","salary":1000000},
  {"id":"1003","firstname":"Somchai","lastname":"Jaidee","company":"Love2work","salary":20000},
  {"id":"1004","firstname":"Monkey D","lastname":"Luffee","company":"One Piece","salary":9000000}
]

for (let row in peopleSalary) {
  let salary = peopleSalary[row].salary;
  let salary2 = salary * 1.1;
  let salary3 = salary2 * 1.1;

  peopleSalary[row].salary = [salary, salary2, salary3];
}

console.log(peopleSalary);