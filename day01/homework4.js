let people = require('./homework4.json');
let color = { "brown": 0, "green": 0, "blue": 0 };
let gender = { "male": 0, "female": 0 };

for (let row in people) {
  switch (people[row].eyeColor) {
    case "brown":
      color.brown++;
      break;
    case "green":
      color.green++;
      break;
    case "blue":
      color.blue++;
  }

  switch (people[row].gender) {
    case "male":
      gender.male++;
      break;
    case "female":
      gender.female++;
  }

  people[row].friendCount = people[row].friends.length;
}

console.log(color);
console.log(gender);
console.log(people);