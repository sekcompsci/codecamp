$(function () {
    var rtime;
    var timeout = false;
    var delta = 100;
    var objChart = '';
    var darkMode = false;
    var timer = setInterval(updateChart, 3000);
    toggleMenu();

    $(window).resize(function () {
        rtime = new Date();

        if (timeout === false) {
            timeout = true;
            setTimeout(resizeend, delta);
        }
    });

    function resizeend() {
        if (new Date() - rtime < delta) {
            setTimeout(resizeend, delta);
        } else {
            timeout = false;
            toggleMenu();
        }
    }

    function toggleMenu() {
        if ($(window).width() < 768) {
            $("#wrapper").removeClass("toggled");
        } else {
            $("#wrapper").addClass("toggled");
        }
    }

    $('.nav-link').click(function () {
        $('.nav-link').removeClass('bg-dark');
        $(this).addClass('bg-dark');
    });

    $('#menu-toggle').click(function () {
        $("#wrapper").toggleClass("toggled");
    });

    $('#setTime').change(function() {
        clearInterval(timer);
        timer = setInterval(updateChart, $('#setTime').val());
        console.log($('#setTime').val());
    });

    function rand() {
        var min = 1;
        var max = 20;
        // and the formula is:
        return (Math.floor(Math.random() * (max - min + 1)) + min)
    }

    $('#addChart').click(function () {
        var time = moment().format('x');

        $("#chartBox").append(`
        <div class="col-md-4">
            <div class="card bottom-space">
                <div class="card-body">
                    <i class="fa fa-trash float-right" aria-hidden="true"></i>
                    <canvas id="myChart${time}"></canvas>
                </div>
            </div>
        </div>
        `)

        var ctx = document.getElementById("myChart" + time);
        var config = darkMode?{
            type: 'bar',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    label: '# of Votes',
                    data: [rand(), rand(), rand(), rand(), rand(), rand()],
                    backgroundColor: [
                        'rgb(189, 195, 199)',
                        'rgb(189, 195, 199)',
                        'rgb(189, 195, 199)',
                        'rgb(189, 195, 199)',
                        'rgb(189, 195, 199)',
                        'rgb(189, 195, 199)'
                    ],
                    borderColor: [
                        'rgb(44, 62, 80)',
                        'rgb(44, 62, 80)',
                        'rgb(44, 62, 80)',
                        'rgb(44, 62, 80)',
                        'rgb(44, 62, 80)',
                        'rgb(44, 62, 80)'
                    ],
                    borderWidth: 1
                }]
            }
        }:{
            type: 'bar',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    label: '# of Votes',
                    data: [rand(), rand(), rand(), rand(), rand(), rand()],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            }
        };
        var chart = new Chart(ctx, config);
        objChart = [...objChart, chart];
    });

    $(document).on('click', '.fa-trash', function(){
        $(this).parent().parent().parent().remove();
    });

    $('#toggleMode').click(function() {
        objChart.map(chart => {
            if(darkMode) {
                chart.data.datasets[0].backgroundColor = [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ];
                chart.data.datasets[0].borderColor = [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ]
            }
            else {
                chart.data.datasets[0].backgroundColor = [
                    'rgb(189, 195, 199)',
                    'rgb(189, 195, 199)',
                    'rgb(189, 195, 199)',
                    'rgb(189, 195, 199)',
                    'rgb(189, 195, 199)',
                    'rgb(189, 195, 199)'
                ];
                chart.data.datasets[0].borderColor = [
                    'rgb(44, 62, 80)',
                    'rgb(44, 62, 80)',
                    'rgb(44, 62, 80)',
                    'rgb(44, 62, 80)',
                    'rgb(44, 62, 80)',
                    'rgb(44, 62, 80)'
                ]
            }

            chart.update();
        });

        darkMode = !darkMode;
    });

    function updateChart() {
        if(objChart !== null && objChart !== undefined && objChart !== '') {
            objChart.map(chart => {
                chart.chart.data.datasets[0].data = [rand(), rand(), rand(), rand(), rand(), rand()];
                chart.update();
            });
        }
    }
});