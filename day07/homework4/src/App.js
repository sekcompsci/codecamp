import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = {
    msg: ''
  }
  componentDidMount() {
    fetch('http://localhost:3333')
    .then(res => {
      return res.json()
    })
    .then(msg => {
      this.setState({ msg: msg })
    })
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          { this.state.msg }
        </p>
      </div>
    );
  }
}

export default App;
