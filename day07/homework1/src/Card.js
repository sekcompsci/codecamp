import React, { Component } from 'react';

class Card extends Component {
    isColor = (color) => {
        alert(`color is: ${color}`)
    }
    render() {
        const { color } = this.props;
        return (
            <div
                className="card"
                style={{ background: color }}
                onClick={() => this.isColor(color)}
            >
            </div>
        );
    }
}

export default Card;