import React from 'react'
import { Card, Layout, Form, Input, Row, Col, Button } from 'antd'

const FormItem = Form.Item
const { Header, Content } = Layout

class RegistrationForm extends React.Component {
    state = {
        confirmDirty: false
    }
    handleSubmit = (e) => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values)
            }
        })
    }
    handleConfirmBlur = (e) => {
        const value = e.target.value
        this.setState({ confirmDirty: this.state.confirmDirty || !!value })
    }
    // validEmail = (rule, value, callback) => {
    //     const form = this.props.form
    //     console.log(form.validateFields);
    // }
    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!')
        } else {
            console.log(value)
            callback()
        }
    }
    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true })
        }
        callback()
    }
    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        }

        return (
            <Layout className="layout">
                <Header style={{ color: '#FFF' }}>Code Camp</Header>
                <Content>
                    <Row>
                        <Col span={12} offset={6}>
                            <Card className="bitcoin" title="Register">
                                <Form onSubmit={this.handleSubmit}>
                                    <FormItem
                                        {...formItemLayout}
                                        label="E-mail"
                                    >
                                        {getFieldDecorator('email', {
                                            rules: [
                                                { type: 'email', message: 'The input is not valid E-mail!' },
                                                { required: true, message: 'Please input your E-mail!' },
                                                // { validator: this.validEmail }
                                            ],
                                        })(
                                            <Input />
                                        )}
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Password"
                                    >
                                        {getFieldDecorator('password', {
                                            rules: [
                                                { required: true, message: 'Please input your password!' },
                                                { min: 6, message: 'Please enter at least 6 characters' },
                                                { validator: this.validateToNextPassword }
                                            ],
                                        })(
                                            <Input type="password" />
                                        )}
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Confirm Password"
                                    >
                                        {getFieldDecorator('confirm', {
                                            rules: [{
                                                required: true, message: 'Please confirm your password!',
                                            }, {
                                                validator: this.compareToFirstPassword,
                                            }],
                                        })(
                                            <Input type="password" onBlur={this.handleConfirmBlur} />
                                        )}
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Phone Number"
                                    >
                                        {getFieldDecorator('phone', {
                                            rules: [
                                                { required: true, message: 'Please input your phone number!' },
                                                { pattern: '^(?:([0-9]{10}))?$', message: 'Invalid format' }
                                            ],
                                        })(
                                            <Input style={{ width: '100%' }} />
                                        )}
                                    </FormItem>
                                    <FormItem {...tailFormItemLayout}>
                                        <Button type="primary" htmlType="submit">Register</Button>
                                    </FormItem>
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                </Content>
            </Layout>
        )
    }
}

const WrappedRegistrationForm = Form.create()(RegistrationForm)
export default WrappedRegistrationForm