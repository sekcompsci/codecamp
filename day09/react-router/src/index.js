import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
// import Homework1 from './homework1'
import Homework2 from './homework2'
import './index.css';

class App extends React.Component {
    render() {
        return (
            // <Homework1 />
            <Homework2 />
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
