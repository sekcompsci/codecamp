import React from 'react'
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom'
import { Layout, Menu } from 'antd'
import PageNotFound from './pagenotfound'

const { Header, Content, Footer } = Layout

class About extends React.Component {
    render() {
        return (
            <div>
                Sirawit Moonrinta (Sek)
            </div>
        )
    }
}

class Job extends React.Component {
    render() {
        return (
            <div>
                Software Engineer
            </div>
        )
    }
}

class Port extends React.Component {
    render() {
        return (
            <div>
                https://www.github.com/mrniranam
            </div>
        )
    }
}

class Skill extends React.Component {
    render() {
        return (
            <div>
                Front End Devoloper & IoT Skill
            </div>
        )
    }
}

class Homework1 extends React.Component {
    render() {
        return (
            <Layout className="layout">
                <BrowserRouter>
                    <div>
                        <Header>
                            <div className="logo" />
                            <Menu
                                theme="dark"
                                mode="horizontal"
                                defaultSelectedKeys={['1']}
                                style={{ lineHeight: '64px' }}
                            >
                                <Menu.Item key="1">
                                    <NavLink exact to="/">About</NavLink>
                                </Menu.Item>
                                <Menu.Item key="2">
                                    <NavLink exact to="/job">Job</NavLink>
                                </Menu.Item>
                                <Menu.Item key="3">
                                    <NavLink exact to="/port">Portfolito</NavLink>
                                </Menu.Item>
                                <Menu.Item key="4">
                                    <NavLink exact to="/skill">Skill</NavLink>
                                </Menu.Item>
                            </Menu>
                        </Header>
                        <Content style={{ padding: '0 50px', marginTop: '1rem' }}>
                            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                                <Switch>
                                    <Route exact path="/" component={About} />
                                    <Route exact path="/job" component={Job} />
                                    <Route exact path="/port" component={Port} />
                                    <Route exact path="/skill" component={Skill} />
                                    <Route exact path="*" component={PageNotFound} />
                                </Switch>
                            </div>
                        </Content>
                        <Footer style={{ textAlign: 'center' }}>
                            React ©2018 Created by MrNiranam
                        </Footer>
                    </div>
                </BrowserRouter>
            </Layout>
        )
    }
}

export default Homework1