import React from 'react'
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom'
import { Layout, Menu } from 'antd'

import Home71 from './day7/homework1'
// import Home72 from './day7/homework2'
import Home81 from './day8/homework1'
import Home82 from './day8/homework2'
import Home83 from './day8/homework3'
import PageNotFound from './pagenotfound'

const { Header, Content, Footer } = Layout

class Home extends React.Component {
    render() {
        return (
            <h1>Hello World!</h1>
        )
    }
}

class Homework2 extends React.Component {
    render() {
        return (
            <Layout className="layout">
                <BrowserRouter>
                    <div>
                        <Header>
                            <div className="logo" />
                            <Menu
                                theme="dark"
                                mode="horizontal"
                                defaultSelectedKeys={['1']}
                                style={{ lineHeight: '64px' }}
                            >
                                <Menu.Item key="1">
                                    <NavLink exact to="/">Home</NavLink>
                                </Menu.Item>
                                <Menu.Item key="2">
                                    <NavLink exact to="/71">Day 7 #1</NavLink>
                                </Menu.Item>
                                <Menu.Item key="3">
                                    <NavLink exact to="/72">Day 7 #2</NavLink>
                                </Menu.Item>
                                <Menu.Item key="4">
                                    <NavLink exact to="/81">Day 8 #1</NavLink>
                                </Menu.Item>
                                <Menu.Item key="5">
                                    <NavLink exact to="/82">Day 8 #2</NavLink>
                                </Menu.Item>
                                <Menu.Item key="6">
                                    <NavLink exact to="/83">Day 8 #3</NavLink>
                                </Menu.Item>
                            </Menu>
                        </Header>
                        <Content style={{ padding: '0 50px', marginTop: '1rem' }}>
                            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                                <Switch>
                                    <Route exact path="/" component={Home} />
                                    <Route exact path="/71" component={Home71} />
                                    {/* <Route exact path="/72" component={Home72} /> */}
                                    <Route exact path="/81/:type?" component={Home81} />
                                    <Route exact path="/82" component={Home82} />
                                    <Route exact path="/83" component={Home83} />
                                    <Route exact path="*" component={PageNotFound} />
                                </Switch>
                            </div>
                        </Content>
                        <Footer style={{ textAlign: 'center' }}>
                            React ©2018 Created by MrNiranam
                        </Footer>
                    </div>
                </BrowserRouter>
            </Layout>
        )
    }
}

export default Homework2