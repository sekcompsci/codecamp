import React, { Component } from 'react';
import './randombox.css';

class RandomBox extends Component {
  render() {
    const color = ['red', 'blue', 'green', 'purple', 'pink'];
    const fontSize = [20, 25, 30, 35, 40];

    return (
      <div className="box" style={{ background: color[Math.floor(Math.random()*color.length)] }}>
          <p className="text" style={{ fontSize: fontSize[Math.floor(Math.random()*fontSize.length)] }}>Random Box</p>
      </div>
    );
  }
}

export default RandomBox;
