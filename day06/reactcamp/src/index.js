import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

//------- Component -------//
// import App from './App';
// import Diy from './Homework2';
// import RandomBox from './homework2';
import Recipe from './homework3';

ReactDOM.render(<Recipe />, document.getElementById('root'));

registerServiceWorker();