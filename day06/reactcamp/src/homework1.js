import React from 'react';

class Div extends React.Component {
    render() {
        return(
            <div>
                <div>hello</div>
                <Button />
            </div>
        )
    }
}

const Button = () => {
    return(<button>click</button>)
}

export default Div;