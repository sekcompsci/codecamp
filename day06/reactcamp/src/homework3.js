import React from 'react';
import './recive.css';

const menus = [
    { 
        id: 1, 
        url: 'https://static.pexels.com/photos/70497/pexels-photo-70497.jpeg',
        name: 'Spaghetti', 
        ingredients: ['pasta', '8 cups water', '1 box spaghetti'], 
        instruction: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy"
    },
    { 
        id: 2, 
        url: 'https://static.pexels.com/photos/70497/pexels-photo-70497.jpeg',
        name: 'Spaghetti', 
        ingredients: ['pasta', '8 cups water', '1 box spaghetti'], 
        instruction: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy"
    },
    { 
        id: 3, 
        url: 'https://static.pexels.com/photos/70497/pexels-photo-70497.jpeg',
        name: 'Spaghetti', 
        ingredients: ['pasta', '8 cups water', '1 box spaghetti'], 
        instruction: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy"
    }
]

class Recive extends React.Component {
    render() {
        return (
            <div>
                <ReciveTop />
                <ReciveContent />
                <ReciveBottom />
            </div>
        )
    }
}

const ReciveTop = () => {
    return (
        <div className="top">
            <span>Recive App</span>
            <div>
                <span>New Recive</span>
                <span>Home</span>
                <span>About</span>
                <span>Contact Us</span>
            </div>
        </div>
    )
}

class ReciveContent extends React.Component {
    state = {
        menus: menus
    }
    render() {
        const { menus } = this.state.menus;
        const item = menus.map(menu => 
            <Item key={menu.id} />
        );

        return (
            <div className="content">
                <form>
                    <label for="search">search:</label>
                    <input id="search" type="text" />
                </form>
                {item}
            </div>
        );
    }
}

class Item extends React.Component {
    render() {
        return (
            <div className="item">
                <div className="card">
                    <img src="https://static.pexels.com/photos/70497/pexels-photo-70497.jpeg" />
                    <div className="card-body">
                        <h3>Spaghetti</h3>
                        <p>Ingredients:</p>
                        <ul>
                            <li>pasta</li>
                            <li>8 cups water</li>
                            <li>1 box spaghetti</li>
                        </ul>
                        <p>Instruction:</p>
                        <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</span>
                    </div>
                </div>
                <div className="card">
                    <img src="https://static.pexels.com/photos/70497/pexels-photo-70497.jpeg" />
                    <div className="card-body">
                        <h3>Spaghetti</h3>
                        <p>Ingredients:</p>
                        <ul>
                            <li>pasta</li>
                            <li>8 cups water</li>
                            <li>1 box spaghetti</li>
                        </ul>
                        <p>Instruction:</p>
                        <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</span>
                    </div>
                </div>
                <div className="card">
                    <img src="https://static.pexels.com/photos/70497/pexels-photo-70497.jpeg" />
                    <div className="card-body">
                        <h3>Spaghetti</h3>
                        <p>Ingredients:</p>
                        <ul>
                            <li>pasta</li>
                            <li>8 cups water</li>
                            <li>1 box spaghetti</li>
                        </ul>
                        <p>Instruction:</p>
                        <span>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</span>
                    </div>
                </div>
            </div>
        );
    }
}

const ReciveBottom = () => {
    return (
        <div className="bottom">this is footer</div>
    )
}

export default Recive;