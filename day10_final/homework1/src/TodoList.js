import React from 'react';
import TodoItem from './TodoItem';

const TodoList = ({ todos, remove_todo }) => (
  <section className="main">
    <ul className="todo-list">
      {todos.map(todo => <TodoItem key={todo.id} todo={todo} remove_todo={remove_todo} />)}
    </ul>
  </section>
);

export default TodoList;
