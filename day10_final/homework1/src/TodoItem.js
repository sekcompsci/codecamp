import React, { Component } from 'react'

export default class TodoItem extends Component {
  render() {
    const { todo, remove_todo } = this.props;
    return (
      <li>
        <div className="view">
          <label>
            {todo.text}
          </label>
          <button className="destroy" onClick={() => remove_todo(todo.id)} />
        </div>
      </li>
    )
  }
}
