import React from 'react';
import Header from './Header';
import TodoList from './TodoList';
import { connect } from 'react-redux';
import { remove_todo } from './actions/todo'

function mapStateToProps(state) {
  return { todos: state.todos }
}

const mapDispatchToProps = {
  remove_todo
}

const App = ({ todos, remove_todo }) => {
    return (
      <div>
        <Header />
        <TodoList todos={ todos } remove_todo={ remove_todo }  />
      </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
