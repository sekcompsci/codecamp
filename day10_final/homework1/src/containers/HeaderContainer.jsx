import { connect } from 'react-redux';
import { add_todo } from '../actions/todo';
import Header from '../components/Header';

const mapDispatchToProps = {
    add_todo
}

export default connect(null, mapDispatchToProps)(Header);