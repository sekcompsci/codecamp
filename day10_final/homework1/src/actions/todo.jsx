export const ADD_TODO = 'ADD_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';

export function add_todo(todo) {
  return { type: ADD_TODO, text: todo };
}

export function remove_todo(id) {
  return { type: REMOVE_TODO, id: id };
}