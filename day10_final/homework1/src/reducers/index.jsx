import { ADD_TODO, REMOVE_TODO } from '../actions/todo'

function rootReducer(state = { todos: [] }, action) {
    switch (action.type) {
        case ADD_TODO:
            return { ...state,
                todos: [
                    ...state.todos,
                    {
                        id: state.todos.reduce((maxId, todo) => Math.max(todo.id, maxId), -1) + 1,
                        text: action.text
                    }
                ]
            }
        case REMOVE_TODO:
            return { ...state, todos: state.todos.filter(todo => todo.id !== action.id) }
        default:
            return state
    }
}

export default rootReducer