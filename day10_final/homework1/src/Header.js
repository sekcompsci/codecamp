import React from 'react';
import { connect } from 'react-redux';
import TodoTextInput from './TodoTextInput';
import { add_todo } from './actions/todo'

const mapDispatchToProps = {
  add_todo
}

const ReduxTodoTextInput = connect(null, mapDispatchToProps)(TodoTextInput)

const Header = () => (
  <header className="header">
    <h1>todos</h1>
    <ReduxTodoTextInput />
  </header>
);

export default Header;
