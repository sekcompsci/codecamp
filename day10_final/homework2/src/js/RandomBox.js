import React, { Component } from 'react'

class RandomBox extends Component {
    render() {
        const arrayColor = ['red', 'blue', 'green', 'purple', 'pink']
        const fontmin = 20
        const fontmax = 40

        const getRandom = (min, max) => {
            return Math.floor(Math.random() * (max - min) + min);
        }

        const randomTxtSize = getRandom(fontmin, fontmax)
        const randomColor = getRandom(0, arrayColor.length)

        const color = arrayColor[randomColor]

        const stylediv = { height: 300, width: 300, backgroundColor: color }
        const stylep = { fontSize: randomTxtSize }

        class Divmain extends Component {
            render() {
                return (
                    <div style={stylediv} align='center' >
                        <p style={stylep}>Random Box</p>
                    </div>
                )
            }
        }

        return (<div>
            <Divmain />
        </div>

        )
    }
}

export default RandomBox