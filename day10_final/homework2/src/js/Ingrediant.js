import React, { Component } from 'react'

class Ingrediant extends Component {

    render() {
        const { no } = this.props
        return (<div>
            {
                no.map(ingrediant => (<div>
                    <input type='text' name='Ingredients' className={ingrediant.index} key={ingrediant.index} value={ingrediant.value} onChange={(e) => this.props.handelChange(e)} /> <br />
                </div>
                ))
            }
        </div>
        )
    }
}

export default Ingrediant 
