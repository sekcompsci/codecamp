import React, { Component } from 'react'

class Headmenu extends Component {
    render() {

        return (<div style={{ overflow: 'auto', color: '#FFFFFF', padding: 8, width: '100%', backgroundColor: '#008080' }}  >
            <span style={{ fontSize: 20 }}>Recipe App</span>
            <div style={{ float: 'right' }}>
                <span onClick={this.props.showForm} style={{ margin: 5, cursor: 'pointer' }}>New Recipe</span>
                <span style={{ margin: 5 }}>Home</span>
                <span style={{ margin: 5 }}>About</span>
            </div>
        </div>
        )
    }
}

export default Headmenu