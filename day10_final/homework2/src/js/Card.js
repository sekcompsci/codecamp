import React, { Component } from 'react'
import imgspaghetti from './img/spaghetti.jpg'

class Card extends Component {
    render() {
        const { Recipe } = this.props
        return (<div>
            {Recipe.map(menu => <div key={menu.Title} style={{ width: '30%', float: 'left', border: '1px solid #000000', margin: 10, padding: 10 , minHeight:500}} >
                <img src={imgspaghetti} style={{ width: '70%' }} />
                <h2>{menu.Title}</h2>
                <h3>Ingredients : </h3>
                <ul>
                    {menu.Ingredients.map(ing => <li>{ing}</li>)}
                </ul>
                <h3>Instructions : </h3>
                <div>{menu.Instructions}</div>
                <br/>
                <button onClick={ () => this.props.funcBtn(menu.Title)}>Delete</button>
            </div>)}
        </div>
        )
    }

}

export default Card