import React, { Component } from 'react'
import Ingrediant from './Ingrediant'
class FormAdd extends Component {
    state = {
        Title: '',
        Ingredients: [{
            index: 1,
            value: ''
        }],
        Instructions: ''
    }
    render() {
        return (<div style={{ width: '100%' }} align='center' >
            <div style={{ width: '30%', border: '1px solid #000000', margin: 10, padding: 0, minHeight: 200 }} >
                <div style={{ width: '100%', backgroundColor: '#008080', height: 30, color: 'white' }} align='left'>
                    Add new recipe
                </div>
                Title <br />
                <input type='text' name='Title' onChange={(e) => this.handelChange(e)} />
                <br /><br />
                Ingrediants <br />
                <Ingrediant no={this.state.Ingredients} handelChange={this.handelChange.bind(this)} />
                <br /><br />
                <button onClick={e => this.showMore()} >Add ingrediant</button>
                <br /><br />
                Instructions <br />
                <textarea name='Instructions' onChange={(e) => this.handelChange(e)} ></textarea>
                <br /><br />
                <button onClick={() => this.props.saveRecipe(this.state)}>Save</button>
                &nbsp;
                <button onClick={() => this.props.closeForm()}>Close</button>
            </div>
        </div>
        )
    }

    showMore() {
        const len = this.state.Ingredients.length
        const newLen = len + 1
        this.setState({
            Ingredients: [...this.state.Ingredients, { index: newLen, value: '' }]
        })
    }

    handelChange(e) {
        const { name, value, className } = e.target
        switch (name) {
            case 'Ingredients':
                this.setState({
                    Ingredients: this.state.Ingredients.map(ing => (
                        (ing.index == className)
                            ? { index: className, value }
                            : ing
                    ))
                })
                break;
            case 'Title':
                this.setState({
                    [name]: value
                })
                break;
            case 'Instructions':
                this.setState({
                    [name]: value
                })
                break;

        }
    }


}

export default FormAdd