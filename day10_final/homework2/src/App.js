import React, { Component } from 'react';
import Card from './js/Card'
import Headmenu from './js/Headmenu'
import Footer from './js/Footer'
import Searchinput from './js/Searchinput'
import FormAdd from './js/FormAdd'
class App extends Component {
  state = {
    Recipe: [{
      Title: 'Spaghetti',
      Ingredients: ['pasta', '8 cups water', '1 box spaghetti'],
      Instructions: 'Open jar of spaghetti . Bring to simmer , Boil water......'
    }, {
      Title: 'Milkshake',
      Ingredients: ['2 scoops Ice cream', '2 bottle of milk'],
      Instructions: 'Shake shake....'
    }, {
      Title: 'Honey Toast',
      Ingredients: ['Bread', 'Butter', 'Honey', 'Ice cream'],
      Instructions: 'Baking bread in oven..'
    }],

    Recipe_Bak: [{
      Title: 'Spaghetti',
      Ingredients: ['pasta', '8 cups water', '1 box spaghetti'],
      Instructions: 'Open jar of spaghetti . Bring to simmer , Boil water......'
    }, {
      Title: 'Milkshake',
      Ingredients: ['2 scoops Ice cream', '2 bottle of milk'],
      Instructions: 'Shake shake....'
    }, {
      Title: 'Honey Toast',
      Ingredients: ['Bread', 'Butter', 'Honey', 'Ice cream'],
      Instructions: 'Baking bread in oven..'
    }]
    ,
    showForm: false
  }

  render() {
    console.log(this.state);
    return (<div>
      <Headmenu showForm={this.showForm} />

      <div align='center' style={{ margin: 10 }}>
        <input type="text" placeholder='Search' onChange={(e) => this.searchState(e)} />
      </div>

      {(this.state.showForm)
        ? <FormAdd saveRecipe={this.saveRecipe} closeForm={this.closeForm} />
        : null}

      <div style={{ overflow: 'auto', minHeight: 500 }} >
        <Card funcBtn={this.deleteMenu} Recipe={this.state.Recipe} />
      </div>

      <Footer />
    </div>
    );
  }

  searchState = (e) => {
    const { Recipe, Recipe_Bak } = this.state
    const { value } = e.target
    let menu = Recipe.filter(menu => menu.Title.toLowerCase().search(value.toLowerCase()) > -1)
    menu = (value == '') ? Recipe_Bak : menu
    this.setState({
      Recipe: menu
    })
  }

  deleteMenu = (Title) => {
    this.setState({
      Recipe: this.state.Recipe.filter(menu => menu.Title !== Title),
      Recipe_Bak: this.state.Recipe_Bak.filter(menu => menu.Title !== Title)
    })
  }

  showForm = () => {
    this.setState({
      showForm: true
    })
  }

  saveRecipe = (value) => {
    const Title = value.Title
    const objIngredients = value.Ingredients
    const Instructions = value.Instructions

    const Ingredients = objIngredients.map(ing => (
      ing.value
    ))

    this.setState({
      Recipe: [...this.state.Recipe, { Title, Ingredients, Instructions }],
      Recipe_Bak: [...this.state.Recipe_Bak, { Title, Ingredients, Instructions }],
      showForm: false
    })
  }

  closeForm = () => {
    this.setState({
      showForm: false
    })
  }
}

export default App;